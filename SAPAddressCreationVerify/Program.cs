﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.SqlClient;
using System.Configuration;
using System.Net.Http;
using System.Net.Mail;
using System.Web.Script.Serialization;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;

namespace SAPAddressCreationVerify
{
    class Program
    {
        public const string URL = "https://onlinetools.ups.com/rest/XAV";
        static void Main(string[] args)
        {
            string q = "";
            UPSRequestData data = new UPSRequestData();

            q = "SELECT prd.KNA1.KUNNR AS Account, prd.KNA1.NAME1, prd.KNA1.NAME2, prd.KNA1.STRAS AS Address, prd.KNA1.ORT01 AS City, prd.KNA1.REGIO AS State, ";
            q += "prd.KNA1.PSTLZ AS Zip, prd.KNA1.LAND1 AS Country, prd.KNA1.TELF1 AS Phone, prd.KNA1.ERDAT AS DateCreated, prd.KNA1.ERNAM AS Createdby, ";
            q += "prd.KNVV.VTWEG AS EXPR3, prd.KNVV.VKBUR AS EXPR4, prd.KNVV.VKGRP AS EXPR5, prd.ADR6.SMTP_ADDR ";
            q += "FROM prd.KNA1 ";
            q += "INNER JOIN prd.KNVV ON prd.KNA1.MANDT = prd.KNVV.MANDT AND prd.KNA1.KUNNR = prd.KNVV.KUNNR ";
            q += "INNER JOIN prd.TVBUR ON prd.KNA1.MANDT = prd.TVBUR.MANDT AND prd.KNVV.VKBUR = prd.TVBUR.VKBUR ";
            q += "INNER JOIN prd.ADR6 ON prd.TVBUR.MANDT = prd.ADR6.CLIENT AND prd.TVBUR.ADRNR = prd.ADR6.ADDRNUMBER ";
            q += "WHERE(prd.KNA1.MANDT = '100') AND(prd.KNA1.ERDAT = '" + DateTime.Now.AddDays(-1).ToString("yyyyMMdd") + "')";

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SAPConnection"].ConnectionString);
            conn.Open();
            SqlCommand cmd = new SqlCommand(q, conn);
            string email;
        
            var results = cmd.ExecuteReader();
            if (results.HasRows)
            {
                while (results.Read())
                {
                    email = results["SMTP_ADDR"].ToString();
                    if (email!=null && email != "")
                    {
                        email = GetCustomerRepresentative(email);
                    }
                    data = new UPSRequestData();
                    data.UPSSecurity = new UPSSecurityGroup();
                    data.XAVRequest = new Transaction();
                    data.XAVRequest.Request = new RequestSettings();
                    data.XAVRequest.AddressKeyFormat = new AddressKey();
                    data.XAVRequest.Request.TransactionReference = new TransReference();
                    data.UPSSecurity.ServiceAccessToken = new ServiceToken();
                    data.UPSSecurity.UsernameToken = new UserToken();
                    data.XAVRequest.Request.TransactionReference.CustomerContext = results["Account"].ToString();
                    data.XAVRequest.AddressKeyFormat.ConsigneeName = results["NAME1"].ToString();
                    data.XAVRequest.AddressKeyFormat.BuildingName = results["NAME2"].ToString();
                    data.XAVRequest.AddressKeyFormat.AddressLine = results["Address"].ToString();
                    data.XAVRequest.AddressKeyFormat.PoliticalDivision2 = results["City"].ToString();
                    data.XAVRequest.AddressKeyFormat.PoliticalDivision1 = results["State"].ToString();
                    data.XAVRequest.AddressKeyFormat.PostcodePrimaryLow = results["Zip"].ToString();
                    data.XAVRequest.AddressKeyFormat.CountryCode = results["Country"].ToString();
                    checkAndUpdateAddress(data, ValidateCall(data),email);
                }
            }
        }

        public static void checkAndUpdateAddress(UPSRequestData data, UPSResponse resp, string SalesRep)
        {
            bool found = false;
            if (resp != null && resp.Fault == null && resp.XAVResponse.Candidate != null && resp.XAVResponse.NoCandidatesIndicator == null && resp.XAVResponse.Response.Error == null)
            {
                foreach (var cand in resp.XAVResponse.Candidate)
                {
                    if (data.XAVRequest.AddressKeyFormat.PoliticalDivision2.ToUpper() == cand.AddressKeyFormat.PoliticalDivision2.ToUpper() &&
                        data.XAVRequest.AddressKeyFormat.PoliticalDivision1.ToUpper() == cand.AddressKeyFormat.PoliticalDivision1.ToUpper() &&
                        data.XAVRequest.AddressKeyFormat.PostcodePrimaryLow.ToUpper() == cand.AddressKeyFormat.PostcodePrimaryLow.ToUpper() + "-" + resp.XAVResponse.Candidate[0].AddressKeyFormat.PostcodeExtendedLow.ToUpper())
                    {
                        found = true;
                    }
                }
                if (found == false && resp.XAVResponse.Candidate.Count() == 1 &&
                    data.XAVRequest.AddressKeyFormat.PoliticalDivision2.ToUpper() == resp.XAVResponse.Candidate[0].AddressKeyFormat.PoliticalDivision2.ToUpper() &&
                data.XAVRequest.AddressKeyFormat.PoliticalDivision1.ToUpper() == resp.XAVResponse.Candidate[0].AddressKeyFormat.PoliticalDivision1.ToUpper() &&
                data.XAVRequest.AddressKeyFormat.PostcodePrimaryLow.ToUpper() != resp.XAVResponse.Candidate[0].AddressKeyFormat.PostcodePrimaryLow.ToUpper() &&
                data.XAVRequest.AddressKeyFormat.PostcodePrimaryLow.ToUpper().Trim() != resp.XAVResponse.Candidate[0].AddressKeyFormat.PostcodePrimaryLow.ToUpper() + "-" + resp.XAVResponse.Candidate[0].AddressKeyFormat.PostcodeExtendedLow.ToUpper())
                {
                    string oldzip = data.XAVRequest.AddressKeyFormat.PostcodePrimaryLow;
                    data.XAVRequest.AddressKeyFormat.PostcodePrimaryLow = resp.XAVResponse.Candidate[0].AddressKeyFormat.PostcodePrimaryLow.ToUpper() + "-" + resp.XAVResponse.Candidate[0].AddressKeyFormat.PostcodeExtendedLow.ToUpper();
                    //Console.WriteLine("Order Updated:  " + AddressInfo.CustomerPO + "From: " + oldzip + "   To:  " + AddressInfo.ShiptoZip);
                    SendUpdatedEmail(oldzip, data.XAVRequest.AddressKeyFormat.PostcodePrimaryLow, data.XAVRequest.Request.TransactionReference.CustomerContext, SalesRep, data.XAVRequest.AddressKeyFormat.AddressLine, data.XAVRequest.AddressKeyFormat.PoliticalDivision2, data.XAVRequest.AddressKeyFormat.PoliticalDivision1, data.XAVRequest.AddressKeyFormat.ConsigneeName);
                }
                else if (!found && resp.XAVResponse.Candidate.Count > 1)
                {
                    SendMultipleCorrectionEmail(resp, data.XAVRequest.AddressKeyFormat.PostcodePrimaryLow, data.XAVRequest.Request.TransactionReference.CustomerContext, SalesRep, data.XAVRequest.AddressKeyFormat.AddressLine, data.XAVRequest.AddressKeyFormat.PoliticalDivision2, data.XAVRequest.AddressKeyFormat.PoliticalDivision1, data.XAVRequest.AddressKeyFormat.ConsigneeName);
                    
                }
            }
            else if (resp.Fault != null)
            {
                SendLookUpErrorEmail(resp, data.XAVRequest.AddressKeyFormat.PostcodePrimaryLow, data.XAVRequest.Request.TransactionReference.CustomerContext, SalesRep, data.XAVRequest.AddressKeyFormat.AddressLine, data.XAVRequest.AddressKeyFormat.PoliticalDivision2, data.XAVRequest.AddressKeyFormat.PoliticalDivision1, data.XAVRequest.AddressKeyFormat.ConsigneeName);
            }


        }

        public static UPSResponse ValidateCall(UPSRequestData data)
        {
            string json = new JavaScriptSerializer().Serialize(data);
            var client = new HttpClient();
            //Console.WriteLine(json.ToString());
            HttpResponseMessage response = client.PostAsync(URL, new StringContent(json, Encoding.UTF8, "application/json")).Result;
            string responseText = PurifyTheUnclean(response.Content.ReadAsStringAsync().Result); //Cleans out arrays on the Address line and combines them to one single string.
            responseText = MakeCandidateArray(responseText);
            //Console.WriteLine(responseText);
            //Console.ReadLine();
            UPSResponse resp = new UPSResponse();

            try
            {
                resp = new JavaScriptSerializer().Deserialize<UPSResponse>(responseText);
            }
            catch (Exception e)
            {
                resp = null;
                Console.WriteLine("Bad Data File " + e.Message);
                Console.WriteLine(json.ToString());
                Console.WriteLine(responseText);
                //Console.ReadLine();
            }
            if (resp == null || resp.Fault != null)
            {
                //Console.WriteLine(json.ToString());
                //Console.WriteLine(responseText);
                //Console.ReadLine();
            }
            /* if (resp.AddressValidationResponse.Response.Error != null)
             {
                 Console.WriteLine(json.ToString());
                 Console.WriteLine(response.Content.ReadAsStringAsync().Result);
                 Console.ReadLine();
             } */
            return resp;
        }
        public static string PurifyTheUnclean(string resp)
        {
            string retval = resp;
            int start = 13;
            int end = 0;
            int separators = 0;
            int prev = 0;
            while (start <= retval.Length && end >= 0 && start >= 13)
            {
                start = retval.IndexOf("AddressLine\":[", prev) + 13;
                end = retval.IndexOf("], \"PoliticalDivision2", start);
                /* Console.WriteLine(start.ToString());
                 Console.WriteLine(end.ToString());
                 Console.WriteLine(prev.ToString());
                 Console.WriteLine(resp);
                 Console.WriteLine(retval);
                 Console.ReadLine();*/

                if (start != 12 && end != -1)
                {
                    separators = retval.IndexOf("\", \"", start, end - start);
                    while (separators != -1)
                    {
                        try
                        {
                            retval = retval.Remove(separators, 4).Insert(separators, " ");
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine();
                            Console.WriteLine();
                            Console.WriteLine(e.Message);
                            Console.WriteLine(resp);
                            Console.WriteLine(retval);
                            Console.ReadLine();
                        }
                        separators = retval.IndexOf("\", \"", start, end - start);
                    }

                }
                prev = start;
            }

            return retval.Replace("\"AddressLine\":[\"", "\"AddressLine\":\"").Replace("\"], \"PoliticalDivision2\":", "\", \"PoliticalDivision2\":");

        }
        public static string MakeCandidateArray(string resp)
        {
            bool foundcandidate = false;
            if (resp.IndexOf("\"Candidate\":{") != -1)
            {
                foundcandidate = true;
                resp = resp.Replace("\"Candidate\":{", "\"Candidate\":[{");
            }
            if (foundcandidate)
            {
                resp = resp.Replace("}}}}", "}}]}}");
            }
            return resp;
        }
        public static void SendUpdatedEmail(string oldzip, string newzip, string CustomerNumber, string SalesRep, string ShiptoAddress, string shiptoCity, string ShiptoState, string ShiptoName)
        {
            SmtpClient smtp = new SmtpClient("192.168.100.18", 25);
            //string defaultRecipient = "mmoore@westchestergear.com";
            string defaultRecipient = SalesRep;
            MailMessage email = new MailMessage();
            email.To.Add(defaultRecipient);
            email.Subject = "Zip Code Discrepancy with Account "+ CustomerNumber;
            email.From = new MailAddress("AddressUpdater@westchestergear.com");

            string message;
            message = "Address for Customer " + CustomerNumber + " was updated:<br />";
            message += ShiptoName + "<br />";
            message += ShiptoAddress + "<br />";
            message += shiptoCity + ", " + ShiptoState + " " + oldzip + "<br /><br />";

            message += "The Recommended Update in SAP is:<br />";
            message += "From:  " + oldzip;
            message += "<br />To:  " + newzip;

            email.Body = message;
            email.IsBodyHtml = true;
            // smtp.Send(email);
        }
        public static void SendMultipleCorrectionEmail(UPSResponse resp, string newzip, string CustomerNumber, string SalesRep, string ShiptoAddress, string shiptoCity, string ShiptoState, string ShiptoName)
        {
            SmtpClient smtp = new SmtpClient("192.168.100.18", 25);
            //string defaultRecipient = "mmoore@westchestergear.com";
            string defaultRecipient = SalesRep;
            MailMessage email = new MailMessage();
            email.To.Add(defaultRecipient);
            email.Subject = "Address Discrepancy With Customer Account " + CustomerNumber;
            email.From = new MailAddress("AddressUpdater@westchestergear.com");

            string message;
            message = "Address for Customer " + CustomerNumber + " may need updated from:<br />";
            message += ShiptoName + "<br />";
            message += ShiptoAddress + "<br />";
            message += shiptoCity + ", " + ShiptoState + " " + newzip + "<br /><br />";

            message += "Possible changes are:<br /> ";
            foreach (var address in resp.XAVResponse.Candidate)
            {
                message += address.AddressKeyFormat.AddressLine + "<br />";
                message += address.AddressKeyFormat.PoliticalDivision2 + ", " + address.AddressKeyFormat.PoliticalDivision1 + " " + address.AddressKeyFormat.PostcodePrimaryLow + "<br /><br />";
            }

            message += "<br /><br />Please correct the Ship To Address in SAP.";

            email.Body = message;
            email.IsBodyHtml = true;
            smtp.Send(email);
        }

        public static void SendLookUpErrorEmail(UPSResponse resp, string newzip, string CustomerNumber, string SalesRep, string ShiptoAddress, string shiptoCity, string ShiptoState, string ShiptoName)
        {
            SmtpClient smtp = new SmtpClient("192.168.100.18", 25);
            //string defaultRecipient = "mmoore@westchestergear.com";
            string defaultRecipient = SalesRep;
            MailMessage email = new MailMessage();
            email.To.Add(defaultRecipient);
            email.Subject = "Address Issue with Customer " + CustomerNumber;
            email.From = new MailAddress("AddressUpdater@westchestergear.com");

            string message;
            message = "Address for Customer " + CustomerNumber + " may need updated:<br />";
            message += ShiptoName + "<br />";
            message += ShiptoAddress + "<br />";
            message += shiptoCity + ", " + ShiptoState + " " + newzip + "<br /><br />";

            message += "The address could not be validated through UPS Validation.  Please verify that the address is correct within SAP.<br />";

            email.Body = message;
            email.IsBodyHtml = true;
            smtp.Send(email);
        }
        public static string GetCustomerRepresentative(string email)
        {
            string uname = email.Split('@')[0];
            //Console.WriteLine(uname);
            DirectoryEntry connectionStart = new DirectoryEntry("LDAP://10.0.0.6");
            DirectorySearcher ds = new DirectorySearcher(connectionStart);
            ds.Filter = "(samaccountname=" + uname + ")";
            SearchResult sr = ds.FindOne();
            if (sr != null)
            {
                DirectoryEntry de = sr.GetDirectoryEntry();
                if (de.Properties.Contains("assistant"))
                {
                    ds = new DirectorySearcher(connectionStart);
                    ds.Filter = "(" + de.Properties["assistant"].Value.ToString().Split(',')[0] + ")";
                    sr = ds.FindOne();
                    if (sr != null)
                    {
                        de = sr.GetDirectoryEntry();
                        return (de.Properties["mail"].Value.ToString());
                    }
                }
            }
            return email;
        }
    }
}
